#!/bin/bash
set -uo pipefail

# Assume this script is in the src directory and work from that location
PROJECT_ROOT="$(cd "$(dirname "$0")" && pwd)"

source "$PROJECT_ROOT/scripts/include.bash"

__DIR__="$(normalise_path ${PROJECT_ROOT})"

IMAGE_NAME="$(get_image_name $__DIR__)"

build_tmp_image "$IMAGE_NAME"

docker run --rm -dt --init -u=$(id -u):$(id -g) --name "$IMAGE_NAME" \
--entrypoint /bin/bash \
-v "$__DIR__/src:/usr/app/src" \
-v "$__DIR__/.eslintrc:/usr/app/.eslintrc" \
-v "$__DIR__/.eslintignore:/usr/app/.eslintignore" \
-v "$__DIR__/.stylelintrc:/usr/app/.stylelintrc" \
"$(get_image_name $__DIR__)"

docker exec "$IMAGE_NAME" npx eslint --quiet --color ./src -c .eslintrc --ext .ts,.tsx,.js,.jsx
exitonfail $? "Eslint failed" "$IMAGE_NAME"

docker exec "$IMAGE_NAME" npx stylelint --color "**/*.{css,scss,sass}"
exitonfail $? "Stylelint failed" "$IMAGE_NAME"

docker stop "$IMAGE_NAME" >> /dev/null 2>&1

echo_success "Static analysis passed"
