#!/bin/bash
set -uo pipefail

# Assume this script is in the src directory and work from that location
PROJECT_ROOT="$(cd "$(dirname "$0")" && pwd)"

source "$PROJECT_ROOT/scripts/include.bash"

__DIR__="$(normalise_path ${PROJECT_ROOT})"

IMAGE_NAME="$(get_image_name $__DIR__)"

build_tmp_image "$IMAGE_NAME"

docker run --rm -dt --init -u=$(id -u):$(id -g) --name "$IMAGE_NAME" \
--entrypoint /bin/bash \
"$(get_image_name $__DIR__)"

ALLOWED_LICENSES=(
    "MIT"
    "Apache-2.0"
    "CC0-1.0"
    "BSD"
    "BSD-2-Clause"
    "BSD-3-Clause"
    "ISC"
    "CC-BY-3.0"
    "CC-BY-4.0"
    "Public Domain"
    "WTFPL"
    "Unlicense"
)

docker exec "$IMAGE_NAME" npx license-checker --onlyAllow \
"$(implode ";" "${ALLOWED_LICENSES[@]}")"
exitonfail $? "License check failed" "$IMAGE_NAME"

docker exec "$IMAGE_NAME" yarn audit
EXIT=$?

docker stop "$IMAGE_NAME" >> /dev/null 2>&1

if [ $EXIT -gt 3 ]; then
    echo_danger "Security audit failed"
    exit 1
fi
if [ $EXIT -gt 0 ]; then
    echo_warning "Security audit passed with warnings"
    exit 1
fi

echo_success "Audit passed"
