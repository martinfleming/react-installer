#!/bin/bash
#############################################################################
#
# A collection of useful bash functions to be used in scripts. To use the
# functions include the line
#
#    source ./scripts/include.bash
#
# in your script
#
#############################################################################


_pushd(){
    command pushd "$@" > /dev/null
}

_popd(){
    command popd "$@" > /dev/null
}

build_tmp_image() {
_pushd "${PROJECT_ROOT}"
docker build -t "$@" . -f-<<EOF
FROM node:12-alpine
# ENV CI=true set in CI run cmd
RUN apk add --no-cache bash
WORKDIR /usr/app
COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn global add license-checker
RUN yarn install
# temp until bug fixed in 3.4.1 with coverage testing
ENV SKIP_PREFLIGHT_CHECK=true
RUN yarn add react-scripts@3.4.0
EOF
_popd
}

get_image_name() {
    echo "tmp-$(basename -- $1)-image"
}

normalise_path() {
    # convert cygwin path
    if [ $(echo "$1" | grep cygdrive) ]; then
        echo "$1" | sed -r -e 's/\/cygdrive\/([a-z])/\1:/g'
        return
    fi
    echo "$1"
}

implode() {
    local IFS="$1";
    shift;
    echo "$*";
}

implode_array() {
    SEP=$1
    shift
    ARRAY=$@
    bar=$(IFS=";" ; echo "$ARRAY")
    echo $(IFS="$SEP" ; echo "${ARRAY[*]}")
}

exitonfail() {
    if [ "$1" -ne "0" ]
    then
        echo_danger "$2"
        IMAGE_NAME="${3:-""}"
        if [ ! -z "${IMAGE_NAME}" ]; then
            docker stop "$IMAGE_NAME" >> /dev/null 2>&1
        fi
        exit 1
    fi
}

echo_colour() {
    colour=$2
    no_colour='\033[0m'
    echo -e "${colour}$1${no_colour}"
}

echo_warning(){
    magenta='\033[0;33;1m'
    echo_colour "$1" "${magenta}"
}

echo_success(){
    green='\033[0;32;1m'
    echo_colour "$1" "${green}"
}

echo_danger(){
  red='\033[0;31;1m'
  echo_colour "$1" "${red}"
}


echo_info(){
  cyan='\033[0;36;1m'
  echo_colour "$1" "${cyan}"
}
