#!/bin/bash
set -uo pipefail

# Assume this script is in the src directory and work from that location
PROJECT_ROOT="$(cd "$(dirname "$0")" && pwd)"

source "$PROJECT_ROOT/scripts/include.bash"

COVERAGE=""

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -c|--coverage)
    COVERAGE="--coverage"
    shift
    ;;
    *)
    shift
    ;;
esac
done

__DIR__="$(normalise_path ${PROJECT_ROOT})"

build_tmp_image "$(get_image_name $__DIR__)"

docker run --rm -t --init \
-v "$__DIR__/tsconfig.json:/usr/app/tsconfig.json" \
-v "$__DIR__/.env:/usr/app/.env" \
-v "$__DIR__/src:/usr/app/src" \
"$(get_image_name $__DIR__)" \
npx react-scripts test "$COVERAGE" --watchAll=false

exitonfail $? "Unit tests failed"

echo_success "Unit tests passed"
