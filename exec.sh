#!/bin/bash

PROJECT_ROOT="$(cd "$(dirname "$0")" && pwd)"

source "$PROJECT_ROOT/scripts/include.bash"

__DIR__="$(normalise_path ${PROJECT_ROOT})"

docker exec -it "$(get_image_name $__DIR__)-run" /bin/bash