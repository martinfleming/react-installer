# Stage 1
FROM node:12-alpine as build
COPY . /app
WORKDIR /app
RUN yarn install && yarn run build

# Stage 2 - add files to server image
FROM registry.gitlab.com/martinfleming/spa-server:latest
COPY --from=build /app/build /var/www/html
