#!/bin/bash
set -uo pipefail

# Assume this script is in the src directory and work from that location
PROJECT_ROOT="$(cd "$(dirname "$0")" && pwd)"

source "$PROJECT_ROOT/scripts/include.bash"

__DIR__="$(normalise_path ${PROJECT_ROOT})"

_pushd $__DIR__

docker build -t "$(get_image_name $__DIR__)-run" . -f-<<EOF
FROM node:12-alpine
RUN apk add --no-cache bash sudo
# bind mounts are owned by root so we will need sudo to allow write
RUN echo node ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/node \
	&& chmod 0440 /etc/sudoers.d/node
RUN yarn global add yalc
RUN echo "Set disable_coredump false" >> /etc/sudo.conf
#  do not know what user id will run container so just relax permissions
RUN mkdir /usr/app && chmod 777 -Rc /usr/app
WORKDIR /usr/app
EOF

VOLUME="$(get_image_name $__DIR__)-volume"
docker volume create "$VOLUME"

docker run --rm -it --init --user $(id -u):$(id -g) \
--network host \
-v "$__DIR__/public:/usr/app/public" \
-v "$__DIR__/src:/usr/app/src" \
-v "$__DIR__/package.json:/usr/app/package.json" \
-v "$__DIR__/yarn.lock:/usr/app/yarn.lock" \
-v "$VOLUME:/usr/app/node_modules" \
"$(get_image_name $__DIR__)-run" \
/bin/bash -c "sudo chmod 777 node_modules && yarn install && yarn run start"
