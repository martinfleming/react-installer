#!/bin/bash

set -e

rm README.md

# create react app in subdir and move files to root
npx create-react-app my-app --template redux-typescript
mv my-app/* .
mv my-app/.[!.]* ./
rmdir my-app

# add extra modules
yarn add \
@types/react-router-dom@^5.1.5 \
@typescript-eslint/eslint-plugin@^2.34.0 \
babel-eslint@^10.1.0 \
eslint@^6.0.0 \
eslint-config-react-app@^5.2.1 \
eslint-plugin-jest@^23.13.1 \
eslint-plugin-react@^7.20.0 \
eslint-plugin-react-hooks@^4.0.4 \
eslint-plugin-security@^1.4.0 \
eslint-plugin-import@^2.21.2 \
eslint-plugin-react-redux@^3.0.3 \
node-sass@^4.14.1 \
react-router-dom@^5.2.0 \
stylelint@^13.5.0 \
stylelint-config-standard@^20.0.0 \
stylelint-scss@^3.17.2 \
react-app-polyfill@^1.0.6 \
postcss@^7.0.32


# modify package.json
node --experimental-modules editConfig.mjs && rm editConfig.mjs

# add polyfil import
sed -i.old "/^import \* as serviceWorker/a import 'react-app-polyfill\/stable';\n" src/index.tsx
chmod --reference src/index.tsx.old src/index.tsx
rm src/index.tsx.old

# delete this file
rm install.sh